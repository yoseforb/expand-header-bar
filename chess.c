/* -*- Mode: C; indent-tabs-mode: nill; c-basic-offset: 8; tab-width: 8 -*- */

/* $ gcc -Wall -o "chess" "chess.c" `pkg-config --libs --cflags gtk+-3.0`
 * or:
 * $ valac --pkg gtk+-3.0 chess.c
 */

#include <gtk/gtk.h>

static GtkWidget *
new_button_with_label (const gchar    *icon_name,
                       const gchar    *str,
                       const gchar    *tooltip,
                       GtkReliefStyle  newstyle,
                       GtkIconSize     size)
{
        GtkWidget *box;
        GtkWidget *image;
        GtkWidget *button;
        GtkWidget *label;

        button = gtk_button_new ();

        box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_style_context_add_class (gtk_widget_get_style_context (box), "linked");

        image = gtk_image_new_from_icon_name (icon_name, size);
        gtk_box_pack_start (GTK_BOX (box), image, FALSE, FALSE, 0);
        if (str)
        {
                label = gtk_label_new_with_mnemonic (str);
                gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 6);
        }

        gtk_container_add (GTK_CONTAINER (button), box);

        gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
        gtk_button_set_relief (GTK_BUTTON (button), newstyle);
        gtk_style_context_add_class (gtk_widget_get_style_context (button), "image-button");
        gtk_widget_set_tooltip_text (button, tooltip);

        return button;
}

int main(int argc, char **argv)
{
        GtkWidget *window;
        GtkWidget *header_bar;
        gboolean   rtl;

        gtk_init (&argc, &argv);

        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
        gtk_window_set_default_size (GTK_WINDOW (window), 800, 600);

        rtl = gtk_widget_get_direction (window) == GTK_TEXT_DIR_RTL;

        /* HeaderBar */
        header_bar = gtk_header_bar_new ();
        gtk_header_bar_set_show_close_button (GTK_HEADER_BAR (header_bar), TRUE);
        gtk_style_context_add_class (gtk_widget_get_style_context (header_bar), "titlebar");
        gtk_header_bar_set_title (GTK_HEADER_BAR (header_bar), "Chess");
        gtk_window_set_titlebar (GTK_WINDOW (window), header_bar);

        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   new_button_with_label ("folder-documents-symbolic",
                                                          NULL,
                                                          "Start a new game",
                                                          GTK_RELIEF_NONE,
                                                          GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   new_button_with_label ("document-open-symbolic",
                                                          NULL,
                                                          "Open a saved game",
                                                          GTK_RELIEF_NONE,
                                                          GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   new_button_with_label ("document-save-symbolic",
                                                          NULL,
                                                          "Save the current game",
                                                          GTK_RELIEF_NONE,
                                                          GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   gtk_separator_new (GTK_ORIENTATION_VERTICAL));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   new_button_with_label (rtl ? "edit-undo-rtl-symbolic" : "edit-undo-symbolic",
                                                          "Undo Move",
                                                          "Undo your most recent move",
                                                          GTK_RELIEF_NONE,
                                                          GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   gtk_separator_new (GTK_ORIENTATION_VERTICAL));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   new_button_with_label ("face-plain-symbolic",
                                                          "Claim Draw",
                                                          "Claim a draw by threefold repetition or the fifty move rule",
                                                          GTK_RELIEF_NONE,
                                                          GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), 
                                   new_button_with_label ("face-sad-symbolic",
                                                          "Resign",
                                                          "Resign to your opponent",
                                                          GTK_RELIEF_NONE,
                                                          GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_end (GTK_HEADER_BAR (header_bar), 
                                 new_button_with_label ("media-playback-pause-symbolic",
                                                        NULL,
                                                        "Pause the game",
                                                        GTK_RELIEF_NONE,
                                                        GTK_ICON_SIZE_MENU));
        gtk_header_bar_pack_end (GTK_HEADER_BAR (header_bar), 
                                 new_button_with_label ("view-fullscreen-symbolic",
                                                        NULL,
                                                        "Toggle fullscreen",
                                                        GTK_RELIEF_NONE,
                                                        GTK_ICON_SIZE_MENU));

        g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

        gtk_widget_show_all (window);

        gtk_main ();

        return 0;
}
