/* -*- Mode: C; indent-tabs-mode: nill; c-basic-offset: 8; tab-width: 8 -*- */

/* $ gcc -Wall -o "main" "main.c" `pkg-config --libs --cflags gtk+-3.0`
 * or:
 * $ valac --pkg gtk+-3.0 main.c
 */

#include <gtk/gtk.h>

int main(int argc, char **argv)
{
        GtkWidget *window;
        GtkWidget *header_bar;
        GtkWidget *box;
        GtkWidget *back_image;
        GtkWidget *back_button;
        GtkWidget *forward_image;
        GtkWidget *forward_button;
        GtkWidget *entry;
        GtkWidget *separator;
        GtkWidget *close_image;
        GtkWidget *close_button;
        GtkWidget *vbox;
        GtkWidget *entry2;
        gboolean   rtl;

        gtk_init (&argc, &argv);

        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
        gtk_window_set_default_size (GTK_WINDOW (window), 680, 400);
        
        rtl = gtk_widget_get_direction (window) == GTK_TEXT_DIR_RTL;

        /* HeaderBar */
        header_bar = gtk_header_bar_new ();
        gtk_window_set_titlebar (GTK_WINDOW (window), header_bar);

        /* Back-Forward button */
        box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_style_context_add_class (gtk_widget_get_style_context (box), "linked");

        /* Back button */
        back_button = gtk_button_new ();
        back_image = gtk_image_new_from_icon_name (rtl ? "go-previous-rtl-symbolic" :
                                                         "go-previous-symbolic",
                                                   GTK_ICON_SIZE_MENU);
        gtk_button_set_image (GTK_BUTTON (back_button), back_image);
        gtk_widget_set_valign (back_button, GTK_ALIGN_CENTER);
        gtk_style_context_add_class (gtk_widget_get_style_context (back_button),
                                     "image-button");
        gtk_box_pack_start (GTK_BOX (box), back_button, FALSE, FALSE, 0);

        /* Forward button */
        forward_button = gtk_button_new ();
        forward_image = gtk_image_new_from_icon_name (rtl ? "go-next-rtl-symbolic" :
                                                            "go-next-symbolic",
                                                      GTK_ICON_SIZE_MENU);
        gtk_button_set_image (GTK_BUTTON (forward_button), forward_image);
        gtk_widget_set_valign (forward_button, GTK_ALIGN_CENTER);
        gtk_style_context_add_class (gtk_widget_get_style_context (forward_button),
                                     "image-button");
        gtk_box_pack_start (GTK_BOX (box), forward_button, FALSE, FALSE, 0);

        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), box);

        /* Entry */
        entry = gtk_entry_new ();
        gtk_widget_set_vexpand (entry, TRUE); /* It does nothing! */
        g_object_set (entry, "expand", TRUE, NULL); /* It also does nothing */
        //~ gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), entry);
        gtk_header_bar_set_custom_title (GTK_HEADER_BAR (header_bar), entry);

        /* Separator */
        separator = gtk_separator_new (GTK_ORIENTATION_VERTICAL);
        gtk_widget_set_valign (separator, GTK_ALIGN_FILL);
        gtk_header_bar_pack_end (GTK_HEADER_BAR (header_bar), separator);

        /* Close button */
        close_button = gtk_button_new ();
        close_image = gtk_image_new_from_icon_name ("window-close-symbolic", GTK_ICON_SIZE_MENU);
        gtk_button_set_image (GTK_BUTTON (close_button), close_image);
        gtk_button_set_relief (GTK_BUTTON (close_button), GTK_RELIEF_NONE);
        gtk_widget_set_valign (close_button, GTK_ALIGN_CENTER);
        gtk_style_context_add_class (gtk_widget_get_style_context (close_button),
                                     "image-button");
        g_signal_connect (close_button, "clicked", G_CALLBACK(gtk_main_quit), NULL);
        gtk_header_bar_pack_end (GTK_HEADER_BAR (header_bar), close_button);

        /* entry2 - expand it is works */
        vbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
        entry2 = gtk_entry_new ();
        gtk_box_pack_start (GTK_BOX (vbox), entry2, TRUE, TRUE, 80);
        //~ gtk_container_add (GTK_CONTAINER (window), vbox);

        g_signal_connect (window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

        gtk_widget_show_all (window);

        gtk_main ();

        return 0;
}
