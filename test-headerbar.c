/* -*- Mode: C; indent-tabs-mode: nill; c-basic-offset: 8; tab-width: 8 -*- */

/* $ gcc -Wall -o "test-headerbar" "test-headerbar.c" `pkg-config --libs --cflags gtk+-3.0`
 * or:
 * $ valac --pkg gtk+-3.0 test-headerbar.c
 */

#include <gtk/gtk.h>

int main(int argc, char **argv)
{
        GtkWidget *window;
        GtkWidget *header_bar;
        GtkWidget *box;
        GtkWidget *back_button;
        GtkWidget *forward_button;
        gboolean   rtl;

        gtk_init (&argc, &argv);

        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
        gtk_window_set_default_size (GTK_WINDOW (window), 680, 400);

        rtl = gtk_widget_get_direction (window) == GTK_TEXT_DIR_RTL;

        /* HeaderBar */
        header_bar = gtk_header_bar_new ();
        gtk_header_bar_set_title (GTK_HEADER_BAR (header_bar), "Test GtkHeaderBar as titlebar");
        gtk_header_bar_set_show_close_button (GTK_HEADER_BAR (header_bar), TRUE);
        gtk_window_set_titlebar (GTK_WINDOW (window), header_bar);

        /* Back-Forward button */
        box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_style_context_add_class (gtk_widget_get_style_context (box), "linked");

        /* Back button */
        back_button = gtk_button_new_from_icon_name (rtl ? "go-previous-rtl-symbolic" :
                                                           "go-previous-symbolic",
                                                     GTK_ICON_SIZE_MENU);
        gtk_widget_set_valign (back_button, GTK_ALIGN_CENTER);
        gtk_box_pack_start (GTK_BOX (box), back_button, FALSE, FALSE, 0);

        /* Forward button */
        forward_button = gtk_button_new_from_icon_name (rtl ? "go-next-rtl-symbolic" :
                                                              "go-next-symbolic",
                                                        GTK_ICON_SIZE_MENU);
        gtk_widget_set_valign (forward_button, GTK_ALIGN_CENTER);
        gtk_box_pack_start (GTK_BOX (box), forward_button, FALSE, FALSE, 0);

        gtk_header_bar_pack_start (GTK_HEADER_BAR (header_bar), box);

        g_signal_connect (window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

        gtk_widget_show_all (window);

        gtk_main ();

        return 0;
}
